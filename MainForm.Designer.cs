﻿namespace SleepTimer
{
    using System.Drawing;

    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.trackBar = new System.Windows.Forms.TrackBar();
            this.hours = new System.Windows.Forms.Label();
            this.minutes = new System.Windows.Forms.Label();
            this.seconds = new System.Windows.Forms.Label();
            this.bStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.hoursLine = new System.Windows.Forms.Label();
            this.minutesLine = new System.Windows.Forms.Label();
            this.secondsLine = new System.Windows.Forms.Label();
            this.contextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.TimerTick);
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenuStrip;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Visible = true;
            this.notifyIcon.BalloonTipClicked += new System.EventHandler(this.BalloonTipClicked);
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon1Click);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выходToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(109, 26);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.ВыходToolStripMenuItemClick);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 60);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(276, 13);
            this.progressBar.TabIndex = 0;
            // 
            // trackBar
            // 
            this.trackBar.Location = new System.Drawing.Point(12, 51);
            this.trackBar.Maximum = 60;
            this.trackBar.Name = "trackBar";
            this.trackBar.Size = new System.Drawing.Size(276, 45);
            this.trackBar.TabIndex = 1;
            this.trackBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBar.Scroll += new System.EventHandler(this.TrackBarScroll);
            // 
            // hours
            // 
            this.hours.BackColor = System.Drawing.SystemColors.Desktop;
            this.hours.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.hours.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hours.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.hours.Location = new System.Drawing.Point(53, 16);
            this.hours.Name = "hours";
            this.hours.Size = new System.Drawing.Size(26, 26);
            this.hours.TabIndex = 2;
            this.hours.Text = "00";
            this.hours.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.hours.TextChanged += new System.EventHandler(this.ValueChanged);
            this.hours.Click += new System.EventHandler(this.TimeboxEnter);
            // 
            // minutes
            // 
            this.minutes.BackColor = System.Drawing.SystemColors.Desktop;
            this.minutes.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.minutes.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minutes.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.minutes.Location = new System.Drawing.Point(94, 16);
            this.minutes.Name = "minutes";
            this.minutes.Size = new System.Drawing.Size(26, 26);
            this.minutes.TabIndex = 3;
            this.minutes.Text = "00";
            this.minutes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.minutes.TextChanged += new System.EventHandler(this.ValueChanged);
            this.minutes.Click += new System.EventHandler(this.TimeboxEnter);
            // 
            // seconds
            // 
            this.seconds.BackColor = System.Drawing.SystemColors.Desktop;
            this.seconds.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.seconds.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.seconds.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.seconds.Location = new System.Drawing.Point(135, 16);
            this.seconds.Name = "seconds";
            this.seconds.Size = new System.Drawing.Size(26, 26);
            this.seconds.TabIndex = 4;
            this.seconds.Text = "00";
            this.seconds.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.seconds.TextChanged += new System.EventHandler(this.ValueChanged);
            this.seconds.Click += new System.EventHandler(this.TimeboxEnter);
            // 
            // bStart
            // 
            this.bStart.FlatAppearance.BorderColor = System.Drawing.SystemColors.HotTrack;
            this.bStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bStart.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bStart.ForeColor = System.Drawing.SystemColors.Window;
            this.bStart.Location = new System.Drawing.Point(186, 16);
            this.bStart.Name = "bStart";
            this.bStart.Size = new System.Drawing.Size(56, 28);
            this.bStart.TabIndex = 5;
            this.bStart.Text = "Go";
            this.bStart.UseVisualStyleBackColor = true;
            this.bStart.Click += new System.EventHandler(this.BStartClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label1.Location = new System.Drawing.Point(79, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 25);
            this.label1.TabIndex = 100;
            this.label1.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label2.Location = new System.Drawing.Point(119, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 25);
            this.label2.TabIndex = 101;
            this.label2.Text = ":";
            // 
            // hoursLine
            // 
            this.hoursLine.BackColor = System.Drawing.SystemColors.HotTrack;
            this.hoursLine.Location = new System.Drawing.Point(53, 42);
            this.hoursLine.Name = "hoursLine";
            this.hoursLine.Size = new System.Drawing.Size(26, 2);
            this.hoursLine.TabIndex = 102;
            // 
            // minutesLine
            // 
            this.minutesLine.BackColor = System.Drawing.SystemColors.HotTrack;
            this.minutesLine.Location = new System.Drawing.Point(94, 42);
            this.minutesLine.Name = "minutesLine";
            this.minutesLine.Size = new System.Drawing.Size(26, 2);
            this.minutesLine.TabIndex = 103;
            // 
            // secondsLine
            // 
            this.secondsLine.BackColor = System.Drawing.SystemColors.HotTrack;
            this.secondsLine.Location = new System.Drawing.Point(135, 42);
            this.secondsLine.Name = "secondsLine";
            this.secondsLine.Size = new System.Drawing.Size(26, 2);
            this.secondsLine.TabIndex = 104;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(300, 104);
            this.ControlBox = false;
            this.Controls.Add(this.secondsLine);
            this.Controls.Add(this.minutesLine);
            this.Controls.Add(this.hoursLine);
            this.Controls.Add(this.bStart);
            this.Controls.Add(this.seconds);
            this.Controls.Add(this.minutes);
            this.Controls.Add(this.hours);
            this.Controls.Add(this.trackBar);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Opacity = 0.95D;
            this.RightToLeftLayout = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Activated += new System.EventHandler(this.MainFormActivated);
            this.Deactivate += new System.EventHandler(this.MainFormDeactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormFormClosing);
            this.contextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.TrackBar trackBar;
        private System.Windows.Forms.Label hours;
        private System.Windows.Forms.Label minutes;
        private System.Windows.Forms.Label seconds;
        private System.Windows.Forms.Button bStart;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label hoursLine;
        private System.Windows.Forms.Label minutesLine;
        private System.Windows.Forms.Label secondsLine;
    }
}

