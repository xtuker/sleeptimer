﻿namespace SleepTimer
{
    using System;
    using System.Windows.Forms;

    public partial class MainForm : Form
    {
        private bool canNotify = false;
        private DateTime sleepTime;
        private Control activeTimeBox;

        public MainForm()
        {
            this.InitializeComponent();
            var desktopWorkingArea = Screen.PrimaryScreen.WorkingArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom - this.Height;
            this.progressBar.Hide();
        }

        private void NotifyIcon1Click(object sender, MouseEventArgs e)
        {
            if (!this.Focused && (e.Button & MouseButtons.Left) != 0)
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
                
            }
        }

        private void MainFormDeactivate(object sender, EventArgs e)
        {
            this.Hide();
            this.WindowState = FormWindowState.Minimized;
        }

        private void BStartClick(object sender, EventArgs e)
        {
            if (this.timer.Enabled)
            {
                this.StopTimer();
                return;
            }
            this.StartTimer();
        }

        private TimeSpan GetTime()
        {
            int.TryParse(this.hours.Text, out int hour);
            int.TryParse(this.minutes.Text, out int min);
            int.TryParse(this.seconds.Text, out int sec);

            return new TimeSpan(hour, min, sec);
        }

        private void TimerTick(object sender, EventArgs e)
        {
            var leftTime = this.sleepTime - DateTime.Now;
            this.Invoke((Action) (() =>
            {
                if (leftTime.TotalSeconds < 0)
                {
                    return;
                }

                this.progressBar.Value = this.progressBar.Maximum - (int) leftTime.TotalSeconds;
                
                this.hours.Text = leftTime.Hours.ToString();
                this.minutes.Text = leftTime.Minutes.ToString();
                this.seconds.Text = leftTime.Seconds.ToString();
                this.notifyIcon.Text = $"Осталось: {leftTime.Hours:D2}:{leftTime.Minutes:D2}:{leftTime.Seconds:D2}";
            }));

            if (this.canNotify && Math.Abs(leftTime.TotalSeconds - 60) < 1)
            {
                this.canNotify = false;
                this.notifyIcon.ShowBalloonTip(5, "Внимание", "Осталась 1 минута", ToolTipIcon.Info);
            }

            if (DateTime.Now >= this.sleepTime)
            {
                this.StopTimer();
                SleepProvider.Sleep();
            }
        }

        private void StartTimer()
        {
            this.trackBar.Hide();
            this.progressBar.Show();
            this.bStart.Text = "Stop";
            this.SetActiveTimeBox(null);

            var time = this.GetTime();

            this.trackBar.Value = 0;
            this.progressBar.Minimum = 0;
            this.progressBar.Maximum = (int) time.TotalSeconds;
            this.progressBar.Value = 0;

            this.sleepTime = DateTime.Now + time;
            this.canNotify = time.TotalSeconds > 60;
            this.timer.Start();
        }

        private void StopTimer()
        {
            this.timer.Stop();
            this.progressBar.Value = 0;
            this.SetActiveTimeBox(this.minutes);
            this.bStart.Text = "Go";
            this.trackBar.Show();
            this.progressBar.Hide();
            this.notifyIcon.Text = "Таймер не запущен";
        }

        private void MainFormActivated(object sender, EventArgs e)
        {
            if (this.timer.Enabled)
            {
                return;
            }

            this.ActiveControl = this.minutes;
            this.SetActiveTimeBox(this.minutes);
        }

        private void TimeboxEnter(object sender, EventArgs e)
        {
            if (this.timer.Enabled)
            {
                return;
            }

            this.SetActiveTimeBox((Control) sender);
        }

        private void TrackBarScroll(object sender, EventArgs e)
        {
            (this.activeTimeBox ?? this.minutes).Text = ((TrackBar)sender).Value.ToString();
        }

        private void ВыходToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainFormFormClosing(object sender, FormClosingEventArgs e)
        {
            this.notifyIcon.Visible = false;
            this.notifyIcon.Dispose();
        }

        private void ValueChanged(object sender, EventArgs e)
        {
            var label = (Label) sender;
            if (int.TryParse(label.Text, out int intValue))
            {
                label.Text = $"{intValue:D2}";
            }
        }

        private void SetActiveTimeBox(Control active)
        {
            this.activeTimeBox = active;

            int.TryParse(active?.Text, out int value);
            switch (active?.Name)
            {
                case "hours":
                    this.hoursLine.Show();
                    this.minutesLine.Hide();
                    this.secondsLine.Hide();
                    this.trackBar.Maximum = 24;
                    break;
                case "minutes":
                    this.hoursLine.Hide();
                    this.minutesLine.Show();
                    this.secondsLine.Hide();
                    this.trackBar.Maximum = 60;
                    break;
                case "seconds":
                    this.hoursLine.Hide();
                    this.minutesLine.Hide();
                    this.secondsLine.Show();
                    this.trackBar.Maximum = 60;
                    break;
                default:
                    this.hoursLine.Hide();
                    this.minutesLine.Hide();
                    this.secondsLine.Hide();
                    break;
            }

            this.trackBar.Value = value;
        }

        private void BalloonTipClicked(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
    }
}
