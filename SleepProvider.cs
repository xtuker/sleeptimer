﻿namespace SleepTimer
{
    using System.Runtime.InteropServices;

    public static class SleepProvider
    {
        [DllImport("powrprof.dll")]
        private static extern int SetSuspendState(int hibernate, int forceCritical, int disableWakeEvent);

        public static void Sleep()
        {
            SleepProvider.SetSuspendState(0, 1, 0);
        }
    }
}