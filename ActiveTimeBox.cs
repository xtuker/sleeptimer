﻿namespace SleepTimer
{
    using System;

    [Flags]
    public enum ActiveTimeBox
    {
        None = 0,
        Hours = 1,
        Minutes = 2,
        Seconds = 4
    }
}