﻿param([string]$TargetPath)
$ProcessName = [system.io.path]::GetFileNameWithoutExtension($TargetPath)
$res = Get-Process -Name "$ProcessName" -FV -ErrorAction Ignore | Select-String -SimpleMatch "$env:userprofile" -Quiet
if($res -eq $True) { 
    Stop-Process -Force -Name "$ProcessName" 
}
cp "$TargetPath" "$env:userprofile" -Force